#How to change Language on edX Platform

###1. Sign in on http://www.transifex.com/ and Join thai team
      create your ~/.transifexrc file:
			hostname = https://www.transifex.com
			username = user
			password = pass
			token =
###2. edit config file in "conf/locale/config.yaml" 
        example {"locales" : ["en_US","th"],"dummy-locale" : "fr"}

###3. create dev_CODE.py in "lms/env" //CODE is language code eg. th,en_US
            example 

            from .dev import *

			USE_I18N = True
			LANGUAGES = ( ('th', 'Thai'), )
			TIME_ZONE = 'Asia/Bangkok'
			LANGUAGE_CODE = 'th'


			if os.path.exists(REPO_ROOT / 'USE_MYSQL'):
			    DATABASES = {
			        'default': {
			            'ENGINE': 'django.db.backends.mysql',
			            'NAME': 'mitxdb',
			            'USER': 'mitxdb',
			            'PASSWORD': 'CYtL6Sj7zdyqryNG',
			            'HOST': '127.0.0.1',
			            'PORT': '3306',
			            }
			        }

###4.Enter command for chang user 
       $source /home/vagrant/.virtualenvs/edx-platform/bin/activate
       Now change mode to
       (edx_platform)vagrant@mitxvm:
       Run command
       $tx pull -l th
       $tx pull -l en_US
###5.Enter code to change language in "conf/local/th/LC_MASSAGES/mako.po"  //change lms mode
      example 
         
         : lms/templates/signup_modal.html:50
			msgid "<i>Welcome</i> {name}"
			msgstr "<i>ยินดีต้อนรับ</i>"
###6.Enter command for generate 
       $rake i18n:generate
       $rake lms[dev_th,0.0.0.0:8004]
###7.Open your browser 192.168.42.2:8004  //lms

###8.If you want to change CMS(Studio) that like lms step
"# This is my README" 

